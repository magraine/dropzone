
// Récupérer les paramètres du formulaire (code repris en pratie de dropzone)
Dropzone.prototype.getFormData = function () {
	var input, inputName, inputType, key, option, value, _k, _l, _len2, _len3, _ref1, _ref2, _ref3, _ref4;
	var data = this.options.params || {};

	if (this.element.tagName === "FORM") {
		_ref2 = this.element.querySelectorAll("input, textarea, select, button");
		for (_k = 0, _len2 = _ref2.length; _k < _len2; _k++) {
			input = _ref2[_k];
			inputName = input.getAttribute("name");
			inputType = input.getAttribute("type");
			if (input.tagName === "SELECT" && input.hasAttribute("multiple")) {
				_ref3 = input.options;
				for (_l = 0, _len3 = _ref3.length; _l < _len3; _l++) {
					option = _ref3[_l];
					if (option.selected) {
						data[inputName] = option.value;
					}
				}
			} else if (!inputType || ((_ref4 = inputType.toLowerCase()) !== "checkbox" && _ref4 !== "radio") || input.checked) {
				data[inputName] = input.value;
			}
		}
	}

	return data;
}


Dropzone.prototype.uploadFiles = function (files) {

	// URL sur laquelle poster le document
	var url    = resolveOption(this.options.url, files);
	// données du formulaire à poster
	var data   = this.getFormData();

	var resumable = new Resumable ({
		target: url,
		maxFiles: Dropzone.prototype.defaultOptions.maxFiles ? Dropzone.prototype.defaultOptions.maxFiles : undefined,
		simultaneousUploads: Dropzone.prototype.defaultOptions.parallelUploads,
		fileParameterName: this.options.paramName,
		query: data,
		testChunks: true
	});

	if (resumable.support) {
		for (var j = 0; j < files.length; j++) {
			var fileLocal = files[j];
			resumable.addFile(fileLocal);
		}

		resumable.on('fileAdded', function (file) {
			resumable.upload();
		});

		resumable.on('fileProgress', function (file) {
			var progressValue = Math.floor(resumable.progress() * 100);
			Dropzone.prototype.defaultOptions.uploadprogress(file.file, progressValue, null);
		});

		resumable.on('fileSuccess', (function(_this) {
			return function (file) {
				return _this._finished([file.file], "success", null);
			}
		})(this));

		resumable.on('error', (function(_this) {
			return function (message, file) {
				return _this._errorProcessing([file.file], message, null);
			}
		})(this));
	} else {
		// Otherwise use the old upload function
		return this.uploadFiles_origin(files);
	}
}
