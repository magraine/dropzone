<?php
/**
 * Utilisations de pipelines par Dropzone
 *
 * @plugin     Dropzone
 * @copyright  2015
 * @author     marcimat
 * @licence    GNU/GPL
 * @package    SPIP\dropzone\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Charger des scripts jquery
 *
 * @pipeline jquery_plugins
 * @param array $scripts Liste à charger
 * @return array Liste complétée
**/
function dropzone_jquery_plugins($scripts) {
	if (test_espace_prive() or lire_config('dropzone/charger_public', false)) {
		include_spip('inc/filtres');
		#$scripts[] = 'lib/resumable/resumable.js';
		#$scripts[] = produire_fond_statique('javascript/dropzone_to_resumable.js');
		$scripts[] = 'lib/flow/flow.js';
		$scripts[] = produire_fond_statique('javascript/dropzone_to_flow.js');
	}
	return $scripts;
}

/**
 * Charger des styles CSS
 *
 * @pipeline insert_head_css
 * @param string $flux Code html des styles CSS à charger
 * @return string Code html complété
**/
function dropzone_insert_head_css($flux) {
	if (test_espace_prive() or lire_config('dropzone/charger_public', false)) {
		 $flux .= '<link rel="stylesheet" href="'.find_in_path('lib/dropzone/dropzone.css').'" type="text/css" media="screen" />';
	}
	return $flux;
}

/**
 * Charger des styles CSS
 *
 * @pipeline insert_head_css
 * @param string $flux Code html des styles CSS à charger
 * @return string Code html complété
**/
function dropzone_header_prive($flux) {
	$flux .= '<link rel="stylesheet" href="'.find_in_path('lib/dropzone/dropzone.css').'" type="text/css" media="screen" />';
	$flux .= '<link rel="stylesheet" href="'.find_in_path('prive/css/dropzone_prive.css').'" type="text/css" media="screen" />';
	return $flux;
}

/**
 * Modifier les formulaires SPIP d'upload
 *
 * @pipeline formulaire_fond
 * @param array $flux Informations du formulaire
 * @return array 
**/
function dropzone_formulaire_fond($flux) {
	if (!in_array($flux['args']['form'], ['joindre_document', 'editer_logo'])) {
		return $flux;
	}

	$objet    = isset($flux['args']['contexte']['objet'])    ? $flux['args']['contexte']['objet']    : '';
	$id_objet = isset($flux['args']['contexte']['id_objet']) ? $flux['args']['contexte']['id_objet'] : 0;

	// Ajouter le cadre / formulaire dropzone au dessus du formulaire habituel pour envoyer des documents
	if ($flux['args']['form'] == 'joindre_document') {
		$dropzone = recuperer_fond('prive/squelettes/inclure/dropzone', [
			'type' => $objet,
			'id' => $id_objet
		]);
		$flux['data'] = $dropzone . $flux['data'];
	}

	// Ajouter le cadre / formulaire dropzone au dessus du formulaire habituel pour envoyer un logo (s'il n'y en a pas déjà un)
	elseif ($flux['args']['form'] == 'editer_logo') {

		$chercher_logo = charger_fonction('chercher_logo','inc');
		if (!$chercher_logo($id_objet, id_table_objet($objet))) {

			// Recharger ce bloc après envoi.
			$ajaxReload = 'navigation';

			// Cas spécial: si on édite le logo du site
			if ($id_objet == 0 and $objet == 'site') {
				$ajaxReload = 'contenu';
			}

			// Récupérer le formulaire d'upload en html5 et lui passer une partie du contexte
			$dropzone = recuperer_fond('prive/squelettes/inclure/dropzone_logo', [
				'type' => $objet,
				'id'   => $id_objet,
				'ajaxReload' => $ajaxReload
			]);

			// Remplacer totalement le formulaire d'origine si la configuration le demande.
			if (lire_config('dropzone/remplacer_logo', false)) {
				$flux['data'] = $dropzone;
			} else {
				$flux['data'] = $dropzone . $flux['data'];
			}
		}
	}

	return $flux;
}
