<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/uploadhtml5/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_charger_public' => 'Script dans l’espace public',
	'cfg_charger_public_case' => 'Charger les scripts sur dans l’espace public',
	'cfg_max_file_size' => 'Taille maximum des fichiers',
	'cfg_max_file_size_explication' => 'Taille maximum des fichiers (en MB).',
	'cfg_max_files' => 'Nombre maximum de fichiers',
	'cfg_max_files_explication' => 'Nombre de maximum de fichiers qui peuvent être uploadés simultanément (0 pour ne pas limiter).',
	'cfg_remplacer_editer_logo' => 'Remplacer logo',
	'cfg_remplacer_editer_logo_case' => 'Remplacer le formulaire d’upload de logo de SPIP',
	'cfg_titre_parametrages' => 'Paramétrages',

	// D
	'drop_annuler' => 'Annuler',
	'drop_annuler_confirm' => 'Annuler cet envoi ?',
	'drop_enlever_fichier' => 'Enlever',
	'drop_fallbacktext' => 'Merci d’utiliser ce formulaire à la place',
	'drop_fichier_invalide' => 'Type de fichier invalide',
	'drop_fichier_trop_gros' => 'Votre fichier est trop volumineux',
	'drop_ici' => 'Déposez vos fichiers ici ou cliquez sur ce cadre',
	'drop_max_file' => 'Nombre maximum de fichiers atteint',
	'drop_no_support' => 'Votre navigateur ne supporte pas le Glisser-Déposer',
	'dropzone_titre' => 'Dropzone',

	// L
	'logo_drop_ici' => 'Déposez votre logo ici ou cliquez sur ce cadre',

	// T
	'titre_page_configurer_dropzone' => 'Configuration de Dropzone',

);

?>
