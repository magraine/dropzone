<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/uploadhtml5/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// U
	'dropzone_description' => 'Intègre la librairie dropzone.js qui permet de téléverser des fichiers par glisser-déposer dans un cadre prévu à cet effet, d\'afficher une miniature des images et la progression du transfert. Intègre également la librairie resumable.js qui permet de téléverser les fichiers de grande taille.',
	'dropzone_nom' => 'Dropzone',
	'dropzone_slogan' => 'Pour téléverser des fichiers facilement'
);

?>
