<?php
/**
 * Fonctions utiles au plugin Dropzone
 *
 * @plugin     Dropzone
 * @copyright  2015
 * @author     marcimat
 * @licence    GNU/GPL
 * @package    SPIP\dropzone\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Intégrer au js dropzone le mappage vers resumable.
 *
 * Tout ça parce que les variables dans la fonction anonyme de dropzone ne sont pas accessibles
 * en dehors… donc plutôt que de toutes les recréer, on modifie le script dropzone tout simplement
 * 
 * @return string Contenu du js dropzone modifié
**/
function dropzone_to_resumable($script = 'resumable') {
	$contenu = $mappage = '';

	$recherche = "Dropzone.prototype.uploadFiles = function(files) {";
	$remplace  = "Dropzone.prototype.uploadFiles_origin = function(files) {";

	lire_fichier(find_in_path('lib/dropzone/dropzone.js'), $contenu);
	lire_fichier(find_in_path('javascript/dropzone_to_' . $script . '.js'), $mappage);

	$contenu = str_replace($recherche, $remplace, $contenu);
	$contenu = str_replace($remplace, "\n" . $mappage . "\n" . $remplace, $contenu);

	return $contenu;
}

/**
 * Intégrer au js dropzone le mappage vers flow.
 *
 * @uses dropzone_to_resumable()
 * @return string Contenu du js dropzone modifié
**/
function dropzone_to_flow() {
	return dropzone_to_resumable('flow');
}

/**
 * Lier les documents téléversés à un objet
 *
 * @param mixed $files $_FILES envoyer par un formulaire had hoc
 * @param mixed $objet
 * @param mixed $id_objet
 * @param string $id_document Dans le cas ou l'on veux remplacer un document.
 * @access public
 */
function dropzone_uploader_document($objet, $id_objet, $files, $id_document='new', $mode = 'auto') {
	include_spip('inc/autoriser');

	/**
	 * Autoriser l'ajout de document ?
	 * 
	 * S'il n'y a pas d'id_objet, c'est qu'on crée un nouveau
	 * document. Les autorisations seront gérées en aval dans
	 * ajouter_document.
	 */
	if ($id_objet AND !autoriser('joindredocument', $objet, $id_objet)) {
		return false;
	}

	// Création du tableau des documents.
	$docs = [];
	foreach ($files as $doc) {
		// pas de fichier vide
		if (!empty($doc['name'])) {
			$docs[] = $doc;
		}
	}

	if (!$docs) {
		return false;
	}

	// Ajouter les documents a un objet SPIP.
	$ajouter_documents = charger_fonction('ajouter_documents', 'action');
	$ajouter_documents(
		$id_document,
		$docs,
		$objet, // Article, rubrique, autre objet
		$id_objet,
		$mode
	);
}

/**
 * Lier un logo téléversé sur un objet
 *
 * @param mixed $objet
 * @param mixed $id_objet
 * @param mixed $fichier
 * @access public
 * @return mixed
 */
function dropzone_uploader_logo($objet, $id_objet, $fichier) {
	include_spip('inc/autoriser');

	// Autorisation de mettre un logo?
	if (!autoriser('iconifier', $objet, $id_objet)) {
		return false;
	}

	include_spip('action/editer_logo');
	return logo_modifier($objet, $id_objet, 'on', $fichier);
}

/**
 * Convertir les formats de logo accepté en mime_type
 *
 * @param mixed $type Liste des formats à convertir en mime type, séparés par une virgule.
 * @param bool $raw
 *     true  : liste en tableau
 *     false : liste en chaîne, séparé par virgule
 * @return string|array
 *     Liste des mimes types
 */
function trouver_mime_type($type, $raw = false) {
	global $formats_logos;

	// Si le type est logo on récupère automatiquement les formats de
	// logo défini par SPIP
	if ($type == 'logo') {
		$type = $formats_logos;
	} else {
		$type = explode(',', $type);
	}

	// On récupère les mimes types demandés par la fonction
	$mime_type = sql_allfetsel('mime_type', 'spip_types_documents', sql_in('extension', $type));
	$mime_type = array_map('reset', $mime_type);

	// Renvoyer le tableau brut
	if ($raw) {
		return $mime_type;
	}

	// Sinon une chaîne
	return implode(',', $mime_type);
}


/**
 * Créer le titre du cadre d'un logo
 * 
 * C'est reprit de prive/formulaires/editer_logo.phpL55
 * Cela devrait être dans une fonction du core de SPIP non ?
 */
function titre_cadre_logo($objet, $id_objet) {
	$balise_img = chercher_filtre('balise_img');
	$img = $balise_img(chemin_image('image-24.png'), "", 'cadre-icone');
	$libelles = pipeline('libeller_logo', isset($GLOBALS['logo_libelles']) ? $GLOBALS['logo_libelles'] : array());
	$libelle = (($id_objet OR $objet != 'rubrique') ? $objet : 'racine');

	if (isset($libelles[$libelle])) {
		$libelle = $libelles[$libelle];
	} elseif ($libelle = objet_info($objet, 'texte_logo_objet')) {
		$libelle = _T($libelle);
	} else {
		$libelle = _L('Logo');
	}

	switch($objet){
		case 'article':
			$libelle .= " " . aide ("logoart");
			break;
		case 'breve':
			$libelle .= " " . aide ("breveslogo");
			break;
		case 'rubrique':
			$libelle .= " " . aide ("rublogo");
			break;
		default:
			break;
	}

	return $img . $libelle;
}



/**
 * Nettoyer un répertoire suivant l'age et le nombre de ses fichiers
 *
 * Nettoie aussi les sous répertoires.
 * Supprime automatiquement les répertoires vides.
 *
 * @note
 *     Attention, cela fait beaucoup d'accès disques.
 * 
 * @param string $repertoire
 *     Répertoire à nettoyer
 * @param int $age_max
 *     Age maxium des fichiers en seconde
 * @param int $max_files
 *     Nombre maximum de fichiers dans le dossier
 * @return bool
 *     - false : erreur de lecture du répertoire.
 *     - true : action réalisée.
 **/
function dropzone_nettoyer_repertoire_recursif($repertoire, $age_max = 24*3600) {
	include_spip('inc/flock');

	$repertoire = rtrim($repertoire, '/');
	if (!is_dir($repertoire)) {
		return false;
	}

	$fichiers = scandir($repertoire);
	if ($fichiers === false) {
		return false;
	}

	$fichiers = array_diff($fichiers, ['..', '.', '.ok']);
	if (!$fichiers) {
		supprimer_repertoire($repertoire);
		return true;
	}

	foreach ($fichiers as $fichier) {
		$chemin = $repertoire . '/' . $fichier;
		if (is_dir($chemin)) {
			dropzone_nettoyer_repertoire_recursif($chemin, $age_max);
		}
		elseif (is_file($chemin) and !jeune_fichier($chemin, $age_max)) {
			supprimer_fichier($chemin);
		}
	}

	// à partir d'ici, on a pu possiblement vider le répertoire…
	// on le supprime s'il est devenu vide.
	$fichiers = scandir($repertoire);
	if ($fichiers === false) {
		return false;
	}

	$fichiers = array_diff($fichiers, ['..', '.', '.ok']);
	if (!$fichiers) {
		supprimer_repertoire($repertoire);
	}

	return true;
}

