<?php
/**
 * Tache de nettoyages de fichiers du plugin Dropzone
 *
 * @plugin     Dropzone
 * @copyright  2015
 * @author     Matthieu Marcillaud
 * @licence    GNU/GPL
 * @package    SPIP\Dropzone\Genie
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Enlève les fichiers du répertoire de travail de dropzone qui sont trop vieux
 *
 * @param int $last
 * @return int
**/
function genie_dropzone_nettoyer_repertoire_upload_dist($last) {

	include_spip('dropzone_fonctions');
	dropzone_nettoyer_repertoire_recursif(_DIR_TMP . 'upload_parts');

	return 1;
}
