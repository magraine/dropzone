<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Dropzone pour ajouter un logo à un objet éditorial
 *
 * @param mixed $objet Objet cible
 * @param mixed $id_objet Id de l'objet cible
 * @param array $options {
 *     @type string $ajaxReload     Objet ajax à recharger quand une image est uploadé
 *     @type string $redirect       Faire une redirection après l'upload de tous les éléménts.
 *     @type string $acceptedFiles  Limiter les types de fichiers acceptés. Une liste d'extension (ex: jpg,gif,pdf)
 *     @type string $paramName      Changer le name du formulaire d'envoi
 *     @type string $id             Changer l'id de la dropzone
 *     @type string $maxFiles       Nombre maximum de fichier envoyés (supplante la configuration globale)
 *     @type string $_header        Placer du texte ou du header dans le haut du formulaire
 *     @type string $_footer        Placer du texte ou du header dans le bas du formulaire
 * }
 * @return array
 *     Valeurs à charger dans le formulaire
 */
function formulaires_dropzone_logo_charger_dist($objet, $id_objet, $options = array()) {
	include_spip("dropzone_fonctions");

	$contexte = [
		'paramName' => 'file_logo',
		'maxFiles' => 1, // un seul fichier
		'acceptedFiles' => trouver_mime_type('logo'), // N'accepter que les logo définis par spip
		'id' => 'dropzonespip_logo' // Un ID spécifique pour les logo
	];

	// Si on est dans l'espace privé, on ajouter le titre du bloc
	if (test_espace_prive() and !isset($args['_header'])) {
		$contexte['_header'] = '<h3 class="titrem">' . titre_cadre_logo($objet, $id_objet) . '</h3>';
	}

	// les options ne doivent pas écraser notre contexte ici
	$contexte = array_merge($options, $contexte);

	return $contexte;
}


/**
 * Vérifier le type de document transmis pour ne pas tout autoriser.
 *
 * @return Liste des erreurs
**/
function formulaires_dropzone_logo_verifier_dist($objet, $id_objet, $options = array()) {
	include_spip("dropzone_fonctions");
	$erreurs = array();

	include_spip("inc/resumable");
	$resumable = new SPIP\Resumable();
	$resumable->run();

	// Ne conserver que les types de documents autorisés.
	foreach ($_FILES as $i => $doc) {
		if (!in_array($doc['type'], trouver_mime_type('logo', true))) {
			unset($_FILES[$i]); // secu
			$erreurs['file_logo'] = _T('dropzone:drop_fichier_invalide');
		}
	}

	return $erreurs;
}

/**
 * Enregistrer le logo transmis
 *
 * @return Liste des erreurs
**/
function formulaires_dropzone_logo_traiter_dist($objet, $id_objet, $options = array()) {
	dropzone_uploader_logo($objet, $id_objet, $_FILES['file_logo']['tmp_name']);

	// Donnée de retour.
	return [
		'editable' => true
	];
}
