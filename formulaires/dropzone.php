<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Dropzone pour ajouter des documents à un objet éditorial
 *
 * @param mixed $objet Objet cible
 * @param mixed $id_objet Id de l'objet cible
 * @param array $options {
 *     @type string $mode           Mode d'ajout des documents (auto, documents, vignette...) ; auto par défaut.
 *     @type string $ajaxReload     Objet ajax à recharger quand une image est uploadé
 *     @type string $redirect       Faire une redirection après l'upload de tous les éléménts.
 *     @type string $acceptedFiles  Limiter les types de fichiers acceptés. Une liste d'extension (ex: jpg,gif,pdf)
 *     @type string $paramName      Changer le name du formulaire d'envoi
 *     @type string $id             Changer l'id de la dropzone
 *     @type string $maxFiles       Nombre maximum de fichier envoyés (supplante la configuration globale)
 *     @type string $_header        Placer du texte ou du header dans le haut du formulaire
 *     @type string $_footer        Placer du texte ou du header dans le bas du formulaire
 * }
 * @return array
 *     Valeurs à charger dans le formulaire
 */
function formulaires_dropzone_charger_dist($objet, $id_objet, $options = array()) {
	include_spip("dropzone_fonctions");

	// Convertir les acceptedFiles
	if (!empty($options['acceptedFiles'])) {
		$options['acceptedFiles'] = trouver_mime_type($options['acceptedFiles']);
	}

	// Contexte de base pouvant être surchargé par $options
	$contexte = [
		'mode'      => 'auto',
		'paramName' => 'file',
		'id'        => 'dropzonespip_'.uniqid()
	];
	$contexte = array_merge($contexte, $options);

	return $contexte;
}


/**
 * Vérifier le type de document transmis pour ne pas tout autoriser.
 *
 * @return Liste des erreurs
**/
function formulaires_dropzone_verifier_dist($objet, $id_objet, $options = array()) {
	include_spip("dropzone_fonctions");
	$erreurs = array();

	include_spip("inc/resumable");
	$resumable = new SPIP\Resumable();
	$resumable->run();

	// Ne conserver que les types de documents autorisés.
	foreach ($_FILES as $i => $doc) {
		if (!empty($options['acceptedFiles'])) {
			if (!in_array($doc['type'], trouver_mime_type($options['acceptedFiles'], true))) {
				unset($_FILES[$i]); // secu
				$erreurs[ isset($options['paramName']) ? $options['paramName'] : 'file' ] = _T('dropzone:drop_fichier_invalide');
			}
		}
	}

	return $erreurs;
}


function formulaires_dropzone_traiter_dist($objet, $id_objet, $options = array()) {
	$mode = isset($options['mode']) ? $options['mode'] : 'auto';
	dropzone_uploader_document($objet, $id_objet, $_FILES, 'new', $mode);

	// Donnée de retour.
	return [
		'editable' => true
	];
}
